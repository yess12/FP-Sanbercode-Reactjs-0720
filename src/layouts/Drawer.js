import React, {  useState, useContext, forwardRef } from 'react';
import {List, ListItem, Collapse, Button, Drawer } from '@material-ui/core';
import clsx from 'clsx';
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import menuItems from './sideBarItems';
import { NavLink as RouterLink } from 'react-router-dom';
// import useStyles from './menuBarStyles';
import { makeStyles } from '@material-ui/styles';
import { colors } from '@material-ui/core';
import {UserContext} from "../context/UserContext"

const MenuBar = (props) => {

    const [user] = useContext(UserContext);
    const useStyles = makeStyles(theme => ({
        root: {
          justifyContent : "left",
        },
        drawer : {
          paddingTop : "20px",
          width: "180px",
        },
        item: {
          display: 'flex',
          paddingTop: 0,
          paddingBottom: 0,
        },
        button: {
          color: colors.blueGrey[800],
          padding: '10px 8px',
          justifyContent: 'flex-start',
          textTransform: 'none',
          letterSpacing: 0,
          width: '100%',
        },
        btnRoot : {
          paddingLeft : "25px",
          justifyContent : "left !important"
        },
        subMenu : {
          paddingLeft : "50px !important",
        }
      }));

    const [ menu, setMenu ] = useState({});
    const { className, ...rest } = props;
    const classes  = useStyles();
    const handleClick = (item) => {
        let newData = {...menu, [item] : !menu[item]};
        setMenu(newData);
    }
    
    const CustomRouterLink = forwardRef((props, ref) => (
      <div ref={ref} style={{ flexGrow: 1 }}>
        <RouterLink {...props} />
      </div>
    ));
    const handleMenu = ( children, level=0 ) => {
      if(user) {
        return children.map(({children, name, url, links }) => {
            if ( !children ) {
              return (
                <List component="div" disablePadding key={ name }>
                  <ListItem
                    className={classes.item}
                    disableGutters
                    style={{padding:"0px"}}
                    key={name}
                  >
                    <Button
                      className={clsx({
                        [classes.btnRoot] : true,
                        [classes.button] : true,
                        [classes.subMenu] : level
                      })}
                      component={CustomRouterLink}
                      to={url}
                    >
                      {name}
                    </Button>
                  </ListItem>
                </List>
              )
            }
            return (
              <div key={ name }>
              <ListItem
                className={classes.item}
                disableGutters
                key={name}
                onClick={() => handleClick(name)}
              >
                <Button
                className={clsx({
                  [classes.btnRoot] : true,
                  [classes.button] : true,
                  [classes.subMenu] : level
                })}>
                  { name } { menu[ name ] ? <ExpandLess /> : <ExpandMore />}
                  </Button>
                </ListItem>
                <Collapse
                  in={ (menu[name]) ? true : false }
                  timeout="auto"
                  unmountOnExit
                >
                  { handleMenu( children, 1) }
                </Collapse>
              </div>
            )
        })
      }
    }

    return (
      <Drawer
          anchor="left"
          classes={{ paper: classes.drawer }}
          open={true}
          variant="persistent"
        >
          <List {...rest} className={clsx(classes.root, className)} >
              { handleMenu(menuItems.data) }
          </List>
      </Drawer>
   )
}

export default MenuBar;