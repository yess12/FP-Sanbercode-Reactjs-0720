import React, { useContext } from "react"
import { UserContext } from "../context/UserContext";
import SwipeableTemporaryDrawer from "./Drawer";
import { useHistory } from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const style = {
  flexGrow: 1,
  marginLeft: 180
}
const styles = {
  borderRadius: 3,
  border: 0,
  color: 'white',
  height: 48,
  padding: '0 10px',
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

const Header =() =>{
  const classes = useStyles();
  let history = useHistory();
  const [user, setUser] = useContext(UserContext)
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
    history.push("/login")
    
  }


  return(    
    <header>
      <AppBar position="fixed" style={{height: 70, background: "black"}}>
        {/* <PersistentDrawerLeft/> */}
        <SwipeableTemporaryDrawer/>
        <Toolbar>
          <Typography variant="h6" style={style}>
              Movies and Games
          </Typography>
          <div className={classes.root}>
              <Button style={styles} color="primary" href="/">Movie List</Button>
              <Button style={styles} color="primary" href="/game-list">Game List</Button>
              { user === null && <Button style={styles} color="primary" href="/register">Register</Button> }
              { user === null && <Button style={styles} color="primary" href="/login">Login</Button> }
              { user && <Button style={styles} color="primary" onClick={handleLogout}>Logout </Button> }
          </div>
        </Toolbar>
      </AppBar>
    </header>
  )
}

export default Header