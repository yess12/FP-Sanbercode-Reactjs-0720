import React, {useContext} from "react"
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import MoviesProvider from "../pages/Movies"
import RegisterProvider from "../pages/Register2"
import Login from "../pages/Login"
import Games from "../pages/Games"
import {UserContext} from "../context/UserContext"
import CreateMovie from "../pages/CreateMovie";
import EditMovie from "../pages/EditMovie"
import CreateGames from "../pages/CreateGame";
import EditGames from "../pages/EditGame"
import ListMovie from "../pages/ListMovie";
import GameList from "../pages/ListGame";
import SingleMovie from "../pages/SingleMovie";
import SingleGame from "../pages/SingleGame";

const Section = () =>{

  const [user] = useContext(UserContext);

  const PrivateRoute = ({login, user, ...props }) => {
    if (login && user) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const LoginRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;

  const RegisterRoute = ({login, ...props}) =>
  login ? <Redirect to="/register" /> : <Route {...props} />

  return(    
    <section >
      <Switch>
        <Route exact path="/" user={user} component={ListMovie}/>
        <Route exact path="/game-list" user={user} component={GameList}/>
        <Route exact path="/register" user={user} component={RegisterProvider} />
        <LoginRoute exact path="/login" user={user} component={Login}/>
        <Route exact path="/movies" user={user} component={MoviesProvider}/>
        <Route exact path="/movies/create" user={user} component={CreateMovie}/>
        <Route exact path="/movies/edit/:id" user={user} component={EditMovie}/>
        <Route exact path="/games" user={user} component={Games}/>
        <Route exact path="/games/create" user={user} component={CreateGames}/>
        <Route exact path="/games/edit/:id" user={user} component={EditGames}/>
        <Route exact path="/movies/:id" user={user} component={SingleMovie}/>
        <Route exact path="/games/:id" user={user} component={SingleGame}/>
      </Switch>
    </section>
  )
}

export default Section