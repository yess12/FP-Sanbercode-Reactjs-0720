export default {
    "data" : [
      {
          "name": "Sign Up",
          "url": "/register"
      },
      {
        "name": "Login",
        "url": "/login"
      },
      {
        "name": "Movie",
        "children": [
          {
            "name": "Movie List",
            "url": "/"
          },
          {
            "name": "Movie List Editor",
            "url": "/movies"
          },
        ]
      },
      {
        "name": "Game",
        "children": [
          {
            "name": "Game List",
            "url": "/game-list"
          },
          {
            "name": "Game List Editor",
            "url": "/games"
          },
        ]
      }
    ]
  }