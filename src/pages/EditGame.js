import React, {useState, useEffect} from "react"
import {useParams} from "react-router-dom"
import axios from "axios"


const EditGames = () => {
  let { id } = useParams();
  const [games, setGames] =  useState(null)
  const [input, setInput]  =  useState({
    name: "",
    genre: "",
    singleplayer: 0,
    multiPlayer: 0,
    platform: "",
    release: 2020,
    image_url: ""
  })

  const handleSubmit = (event) =>{
    axios.put(`https://backendexample.sanbersy.com/api/games/${id}`, {
      name: input.name,
      genre: input.genre,
      singlePlayer: input.singlePlayer,
      multiplayer: input.multiplayer,
      platform: input.platform,
      release: input.release,
      image_url: input.image_url
    })
    .then(res => {
        let singleGame = games.find(el=> el.id === id)
        singleGame.name = input.name
        singleGame.genre = input.genre
        singleGame.singlePlayer = input.singlePlayer
        singleGame.multiplayer = input.multiplayer
        singleGame.platform = input.platform
        singleGame.relase = input.release
        singleGame.image_url = input.image_url
        setGames([...games])
    })
  }

  useEffect( () => {
    if (games === null){
      axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
      .then(res => {
          setGames(res.data)
      })
    }
  }, [games])
  
  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "name":
      {
        setInput({...input, name: event.target.value});
        break
      }
      case "genre":
        {
          setInput({...input, genre: event.target.value});
            break
        }
      case "singlePlayer":
      {
        setInput({...input, singlePlayer: event.target.value});
        break
      }
      case "multiplayer":
      {
        setInput({...input, multiplayer: event.target.value});
          break
      }
      case "platform":
      {
        setInput({...input, platform: event.target.value});
          break
      }
      case "release":
        {
          setInput({...input, release: event.target.value});
            break
        }
      case "image_url":
        {
          setInput({...input, image_url: event.target.value});
            break
        }
    default:
      {break;}
    }
  }

  return(
    <>
      {games !== null &&
          <>
              <h1>{games.name}</h1>
              <p>Genre: {games.genre}</p>
              <p>Singleplayer: {games.singlePlayer}</p>
              <p>Multiplayer: {games.multiplayer}</p>
              <p>Platform: {games.platform}</p>
              <p>Release: {games.release}</p>
              <p><img width="300px" src={games.image_url}/></p>

              <h1>Edit Game</h1>
              <form onSubmit={handleSubmit}>
                <div>
                  <label style={{float: "left"}}>
                    name:
                  </label>
                  <input style={{float: "right"}} type="text" name="name" value={input.name} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div>
                  <label style={{float: "left"}}>
                    genre:
                  </label>
                  <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div>
                  <label style={{float: "left"}}>
                    singlePlayer:
                  </label>
                  <input style={{float: "right"}} type="number" name="singlePlayer" value={input.singlePlayer} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    multiplayer:
                  </label>
                  <input style={{float: "right"}} type="number"  name="multiplayer" value={input.multiplayer} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    platform:
                  </label>
                  <input style={{float: "right"}} type="text" name="platform" value={input.platform} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Release:
                  </label>
                  <input style={{float: "right"}} type="number" name="release" value={input.release} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    image_url:
                  </label>
                  <input style={{float: "right"}} type="text" name="image_url" value={input.image_url} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <button>submit</button>
              </form>
          </>
          }
      </>
  )
}
export default EditGames