import React, { useState, useEffect } from "react";
import {useParams} from "react-router-dom"
import axios from "axios"

const EditMovie = () => {
  let { id } = useParams();
  const [movie, setMovie] = useState(null)
  const [input, setInput]  =  useState({
    title: "",
    description: "",
    year: 2020,
    duration: 120,
    genre: "",
    rating: 0,
    review: "",
    image_url: ""
  })

  const handleSubmit = (event) => {
    axios.put(`https://www.backendexample.sanbersy.com/api/movies/${id}`, {
      title: input.title,
      description: input.description,
      year: input.year,
      duration: input.duration,
      genre: input.genre,
      rating: parseInt(input.rating),
      review: input.review,
      image_url: input.image_url
    })
    .then(res => {
        let singleMovie = movie.find(el=> el.id === id)
        singleMovie.title = input.title
        singleMovie.description = input.description
        singleMovie.year = input.year
        singleMovie.duration = input.duration
        singleMovie.genre = input.genre
        singleMovie.rating = input.rating
        singleMovie.review = input.review
        singleMovie.image_url = input.image_url
        setMovie([...movie])
    })
  }

  useEffect( () => {
      if (movie === null){
        axios.get(`https://www.backendexample.sanbersy.com/api/movies/${id}`)
        .then(res => {
              setMovie(res.data)
        })
      }
  })

  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "title":
      {
        setInput({...input, title: event.target.value});
        break
      }
      case "description":
      {
        setInput({...input, description: event.target.value});
        break
      }
      case "year":
      {
        setInput({...input, year: event.target.value});
          break
      }
      case "duration":
      {
        setInput({...input, duration: event.target.value});
          break
      }
      case "genre":
        {
          setInput({...input, genre: event.target.value});
            break
        }
      case "rating":
        {
          setInput({...input, rating: event.target.value});
            break
        }
      case "review":
        {
          setInput({...input, review: event.target.value});
            break
        }
      case "image_url":
        {
          setInput({...input, image_url: event.target.value});
            break
        }
    default:
      {break;}
    }
  }

  return(
      <>
          {movie !== null &&
          <>
              <h1>{movie.title}</h1>
              <p>Deskripsi: {movie.description}</p>
              <p>Tahun: {movie.year}</p>
              <p>Durasi: {movie.duration}</p>
              <p>Genre: {movie.genre}</p>
              <p>Rating: {movie.rating}</p>
              <p>Review: {movie.review}</p>
              <p><img width="300px" src={movie.image_url}/></p>

              <h1>Edit Movie</h1>
              <form onSubmit={handleSubmit}>
                <div>
                  <label style={{float: "left"}}>
                    Title:
                  </label>
                  <input style={{float: "right"}}  type="text" name="title" value={input.title} onChange={handleChange}/>
                </div>
                <br/>
                <br/>
                <div>
                  <label style={{float: "left"}}>
                    Description:
                  </label>
                  <textarea style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Year:
                  </label>
                  <input style={{float: "right"}} type="number" max={2020} min={1980}  name="year" value={input.year} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Duration:
                  </label>
                  <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Genre:
                  </label>
                  <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Rating:
                  </label>
                  <input style={{float: "right"}} type="number" max={10} min={0} name="rating" value={input.rating} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Review:
                  </label>
                  <input style={{float: "right"}} type="text" name="review" value={input.review} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <div style={{marginTop: "20px"}}>
                  <label style={{float: "left"}}>
                    Image_url:
                  </label>
                  <input style={{float: "right"}} type="text" name="image_url" value={input.image_url} onChange={handleChange}/>
                  <br/>
                  <br/>
                </div>
                <br/>
                <br/>
                <button>submit</button>
              </form>
          </>
          }

      </>
  )
}

export default EditMovie
