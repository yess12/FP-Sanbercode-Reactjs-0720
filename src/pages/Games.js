import React, {useState, useEffect, createContext } from "react"
import axios from "axios"
import { useHistory } from "react-router-dom"
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'no', numeric: true, disablePadding: true, label: 'No'},
  { id: 'name', numeric: false, disablePadding: true, label: 'Title' },
  { id: 'genre', numeric: false, disablePadding: false, label: 'Description' },
  { id: 'singlePlayer', numeric: true, disablePadding: false, label: 'Year' },
  { id: 'multiplayer', numeric: true, disablePadding: false, label: 'Duration' },
  { id: 'platform', numeric: false, disablePadding: false, label: 'Genre' },
  { id: 'release', numeric: true, disablePadding: false, label: 'Rating' },
  { id: 'image_url', numeric: false, disablePadding: true, label: 'Review' },
  { id: 'action', numeric: false, disablePadding: true, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          Nutrition
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));


const Games = () => {
  
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [games, setGames] =  useState([])
  const [input, setInput]  =  useState({
    name: "",
    genre: "",
    singleplayer: "",
    multiPlayer: "",
    platform: "",
    release: "",
    image_url: ""
  })
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")
  const [inputFilter, setInputFilter] = useState({singleStart: 0, singleEnd: 0, multiStart: 0, multiEnd: 0, releaseStart: 0, releaseEnd: 2020})
  const [inputSearch, setInputSearch] = useState("")

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffect( () => {
    if (games === null){
      axios.get(`https://backendexample.sanbersy.com/api/games`)
      .then(res => {
          setGames(res.data.map(el=>{ return {
            id: el.id,
            name: el.name, 
            genre: el.genre, 
            singlePlayer: el.singlePlayer,
            multiplayer: el.multiplayer,
            platform: el.platform,
            release: el.release,
            image_url: el.image_url,
          }
        }))
      })
    }
  }, [games])

  const submitFilter = (e) => {
    e.preventDefault();
    axios.get(`https://backendexample.sanbersy.com/api/games`)
      .then(res => {
          let resGames = res.data.map(el=>{ return {
            id: el.id,
            name: el.name, 
            genre: el.genre, 
            singlePlayer: el.singlePlayer,
            multiplayer: el.multiplayer,
            platform: el.platform,
            release: el.release,
            image_url: el.image_url,
          }
        })
        let filteredGames = resGames.filter(
            x=> x.singlePlayer >= inputFilter.singleStart && x.singlePlayer <= inputFilter.singleEnd &&
            x.multiplayer >= inputFilter.multiStart && x.multiplayer <= inputFilter.multiEnd &&
            x.release >= inputFilter.releaseStart && x.release <= inputFilter.releaseEnd
        )
        setGames([...filteredGames])
      })
  }
  
  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "name":
      {
        setInput({...input, name: event.target.value});
        break
      }
      case "genre":
        {
          setInput({...input, genre: event.target.value});
            break
        }
      case "singlePlayer":
      {
        setInput({...input, singlePlayer: event.target.value});
        break
      }
      case "multiplayer":
      {
        setInput({...input, multiplayer: event.target.value});
          break
      }
      case "platform":
      {
        setInput({...input, platform: event.target.value});
          break
      }
      case "release":
        {
          setInput({...input, release: event.target.value});
            break
        }
      case "image_url":
        {
          setInput({...input, image_url: event.target.value});
            break
        }
    default:
      {break;}
    }
  }

  const handleSubmit = (event) =>{
    event.preventDefault()

    let name = input.name
    console.log(input)

    if (name.replace(/\s/g,'') !== ""){      
      if (statusForm === "create"){        
        axios.post(`https://backendexample.sanbersy.com/api/games`, {
          name: input.name,
          genre: input.genre,
          singlePlayer: input.singlePlayer,
          multiplayer: input.multiplayer,
          platform: input.platform,
          release: input.release,
          image_url: parseInt(input.image_url)
        })
        .then(res => {
            setGames([...games, {id: res.data.id, ...input}])
        })
      }else if(statusForm === "edit"){
        axios.put(`https://backendexample.sanbersy.com/api/games/${selectedId}`, {
          name: input.name,
          genre: input.genre,
          singlePlayer: input.singlePlayer,
          multiplayer: input.multiplayer,
          platform: input.platform,
          release: input.release,
          image_url: parseInt(input.image_url)
        })
        .then(res => {
            let singleGame = games.find(el=> el.id === selectedId)
            singleGame.name = input.name
            singleGame.genre = input.genre
            singleGame.singlePlayer = input.singlePlayer
            singleGame.multiplayer = input.multiplayer
            singleGame.platform = input.platform
            singleGame.relase = input.release
            singleGame.image_url = input.image_url
            setGames([...games])
        })
      }
      
      setStatusForm("create")
      setSelectedId(0)
      setInput({
        name: "",
        genre: "",
        singlePlayer: "",
        multiplayer: "",
        platform: "",
        release: "",
        image_url: ""
      })
    }

  }

  const Action = ({itemId}) =>{
    const handleDelete = () => {  
      let newGames = games.filter(el => el.id != itemId)
  
      axios.delete(`https://backendexample.sanbersy.com/api/games/${itemId}`)
      .then(res => {
        console.log(res)
      })
            
      setGames([...newGames])
      
    }
    
    let history = useHistory();
    const handleEdit = () =>{
      let singleGame = games.find(x=> x.id === itemId)
      setInput({
        name: singleGame.name,
        genre: singleGame.genre,
        singlePlayer: singleGame.singlePlayer,
        multiplayer: singleGame.multiplayer,
        platform: singleGame.platform,
        release: singleGame.release,
        image_url: singleGame.image_url
      })
      setSelectedId(itemId)
      setStatusForm("edit")
      history.push("/games/edit/" + itemId)
    }

    return(
      <>
        <button type="button" onClick={handleEdit}>Edit</button>
        &nbsp;
        <button onClick={handleDelete}>Delete</button>
      </>
    )
  }

  let history = useHistory();
  const createGame = (e) => {
    e.preventDefault();
    history.push('games/create')

    return(
      <button onClick={createGame}>Add Game</button>
    )
  }

  const handleChangeFilter = (e) => {
    switch(e.target.name) {
      case "singleStart": {
        setInputFilter({...inputFilter, singleStart: e.target.value})
        break;
      }
      case "singleEnd": {
        setInputFilter({...inputFilter, singleEnd: e.target.value})
        break;
      }
      case "multiStart": {
        setInputFilter({...inputFilter, multiStart: e.target.value})
        break;
      }
      case "multiEnd": {
        setInputFilter({...inputFilter, multiEnd: e.target.value})
        break;
      }
      case "releaseStart": {
        setInputFilter({...inputFilter, releaseStart: e.target.value})
        break;
      }
      case "releaseEnd": {
        setInputFilter({...inputFilter, releaseEnd: e.target.value})
        break;
      }
      default: {
        break;
      }
    }
  }

  const submitSearch = (e) => {
    e.preventDefault();
    axios.get(`https://backendexample.sanbersy.com/api/games`)
      .then(res => {
          let resGames = res.data.map(el=>{ return {
            id: el.id,
            name: el.name, 
            genre: el.genre, 
            singlePlayer: el.singlePlayer,
            multiplayer: el.multiplayer,
            platform: el.platform,
            release: el.release,
            image_url: el.image_url,
          }
        })
        let searchedGames = resGames.filter(
          x=> x.name.toLowerCase() === inputSearch.toLowerCase()
        ) 
        setGames([...searchedGames])
    })
  }

  const handleChangeSearch = (e) => {
    setInputSearch(e.target.value)
  }

  return(
    <>
    <div>
      <form onSubmit={submitSearch}>
      <label>Search Game Name</label>
        <input name="name" type="text" onChange={handleChangeSearch} value={inputSearch}/>
        <button>search</button>
      </form>
    </div>
    <br/>
    <div>
      <form onSubmit={submitFilter}>
        <label>Single Player</label>
        <input name="singleStart" type="number" onChange={handleChangeFilter} value={inputFilter.singleStart}/>
        <label> - </label>
        <input name="singleEnd" type="number" onChange={handleChangeFilter} value={inputFilter.singleEnd}/>
        <br/>
        <label>Multiplayer</label>
        <input name="multiStart" type="number" onChange={handleChangeFilter} value={inputFilter.multiStart}/>
        <label> - </label>
        <input name="multiEnd" type="number" onChange={handleChangeFilter} value={inputFilter.multiEnd}/>
        <br/>
        <label>Release</label>
        <input name="releaseStart" type="number" onChange={handleChangeFilter} value={inputFilter.releaseStart}/>
        <label> - </label>
        <input name="releaseEnd" type="number" onChange={handleChangeFilter} value={inputFilter.releaseEnd}/>
        <button>Filter</button>
        <br/> <br/>
        <button>Show Game</button>
      </form>
    </div>
    <div>
      <form style={{display: "inline"}} onSubmit={createGame}>
        <button>Add Game</button>
      </form>
    </div>
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={games.length}
            />
            <TableBody>
              {games !== null && stableSort(games, getComparator(order, orderBy))
                .map((item, index) => {

                  return (
                    <TableRow
                      hover
                      tabIndex={-1}
                      key={index}
                    >
                      <TableCell>{index+1}</TableCell>
                      <TableCell>{item.name}</TableCell>
                      <TableCell>{item.genre}</TableCell>
                      <TableCell>{item.singlePlayer}</TableCell>
                      <TableCell>{item.multiplayer}</TableCell>
                      <TableCell>{item.platform}</TableCell>
                      <TableCell>{item.release}</TableCell>
                      <TableCell><img width="100px" src={item.image_url}/></TableCell>
                      <TableCell>
                        <Action itemId={item.id} />
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
    </>
  )
}


export default Games