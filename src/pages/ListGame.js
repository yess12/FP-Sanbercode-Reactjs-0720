import React, {useState, useEffect} from "react"
import axios from "axios"
import "./Movies.css"
import {useHistory} from "react-router-dom"
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Paper } from "@material-ui/core";
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({

  roots: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 200,
    marginTop: 20
  },
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    marginTop: 20
  },
  paper2: {
    paddingBottom: 50,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    padding: theme.spacing(2, 2, 0),
  },
  paper2: {
    paddingBottom: 50,

  },
  list: {
    marginBottom: theme.spacing(2),
  },
  subheader: {
    backgroundColor: theme.palette.background.paper,
  },
  appBar: {
    top: 'auto',
    bottom: 0,
  },
  grow: {
    flexGrow: 1,
  },
  fabButton: {
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: '0 auto',
  },
}));


const GameList = () => {
  const classes = useStyles();
  // let { id } = useParams();
  const [games, setGames] =  useState(null)
  const [input, setInput]  =  useState({
    name: "",
  })
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")
  const [inputSearch, setInputSearch] = useState("")
  const [open, setOpen] = React.useState(false);
  const [number, setNumber] = useState(null)


  useEffect( () => {
    if (games === null){
      axios.get(`https://www.backendexample.sanbersy.com/api/games`)
      .then(res => {
          setGames(res.data.map(el=>{ return {
            id: el.id, 
            name: el.name,
            release: el.release,
            image_url: el.image_url 
          }
        }))
      })
    }
  }, [games])

  const Action = ({itemId}) => {
    let history = useHistory();
    const detail = () =>{
      let singleGame = games.find(x=> x.id === itemId)
      setInput({
        name: singleGame.name,
        genre: singleGame.genre,
        singlePlayer: singleGame.singlePlayer,
        multiplayer: singleGame.multiplayer,
        platform: singleGame.platform,
        release: singleGame.release,
        image_url: singleGame.image_url
      })
      setSelectedId(itemId)
      history.push("/games/" + itemId)
      console.log(singleGame.name)
    }

    return(
      <>
        <Button variant="contained" size="small" onClick={detail}>
          Details
        </Button>
      </>
    )
  }

  const submitSearch = (e) => {
    e.preventDefault();
    axios.get(`https://backendexample.sanbersy.com/api/games`)
      .then(res => {
          let resGames = res.data.map(el=>{ return {
            id: el.id,
            name: el.name, 
            genre: el.genre, 
            singlePlayer: el.singlePlayer,
            multiplayer: el.multiplayer,
            platform: el.platform,
            release: el.release,
            image_url: el.image_url,
          }
        })
        let searchedGames = resGames.filter(
          x=> x.name.toLowerCase() === inputSearch.toLowerCase()
        ) 
        setGames([...searchedGames])
    })
  }

  const handleChangeSearch = (e) => {
    setInputSearch(e.target.value)
  }

  return(
    <>
    <Typography style={{textAlign: "center", fontSize: 40}}>Games Review</Typography>
    <Paper component="form" onSubmit={submitSearch} className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search Game"
        name="name"
        inputProps={{ 'aria-label': ' description ' }}
        onChange={handleChangeSearch}
        value={inputSearch}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton type="submit" className={classes.iconButton} aria-label="search">
        <SearchIcon onSubmit={submitSearch}/>
      </IconButton>
    </Paper>
    <div className={classes.root2} style={{ marginTop: 20, paddingTop: 10, marginRight: 130, marginLeft: 130 }}>
          <List className={classes.list}>
          {games !== null && games.map((item, index)=>{
              return(
              <React.Fragment key={index}>
                  <Container style={{borderWidth: 10, borderColor: "black"}} fixed>
                      <ListItem button>
                          <img width="250" alt="Game Picture" src={item.image_url} />
                          <ListItemText style={{paddingLeft: 20}} primary={item.name} secondary={item.release} />
                          <Action itemId={item.id}/>
                      </ListItem>
                  </Container>
              </React.Fragment>
              )
          })}
          </List>
    </div>
    </>
  );
}

export default GameList