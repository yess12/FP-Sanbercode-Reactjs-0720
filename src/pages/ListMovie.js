import React, {useState, useEffect} from "react"
import axios from "axios"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
  } from "react-router-dom";
import "./Movies.css"
import SingleMovie from "./SingleMovie";
import {useHistory} from "react-router-dom"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Grid, Paper } from "@material-ui/core";
import {sizing} from '@material-ui/system';
import Input from '@material-ui/core/Input';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
  root: {
    // color: theme.palette.text.primary,
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    marginTop: 20,
  },
  roots: {
    maxWidth: 345,
    // '& > *': {
    //   margin: theme.spacing(1),
    //   width: '25ch',
    // },
  },
    input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const ListMovie2 = () => {
  const classes = useStyles();
//   let { id } = useParams();
  const [movies, setMovies] =  useState(null)
  const [input, setInput]  =  useState({
    title: "",
    description: "",
  })
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")
  const [inputSearch, setInputSearch] = useState("")

  useEffect( () => {
    if (movies === null){
      axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
      .then(res => {
          setMovies(res.data.map(el=>{ return {
            id: el.id, 
            title: el.title, 
            description: el.description,
            year: el.year,
            image_url: el.image_url
          }
        }))
      })
    }
  }, [movies])

  const Action = ({itemId}) => {
    let history = useHistory();
    const detail = () =>{
      let singleMovie = movies.find(x=> x.id === itemId)
      setInput({
        title: singleMovie.title,
        description: singleMovie.description,
        year: singleMovie.year,
        duration: singleMovie.duration,
        genre: singleMovie.genre,
        rating: singleMovie.rating,
        review: singleMovie.review,
        image_url: singleMovie.image_url
      })
      setSelectedId(itemId)
      setStatusForm("detail")
      history.push("/movies/" + itemId)
    }

    return(
      <>
        <Button size="small" color="primary" onClick={detail}>
          Details
        </Button>
      </>
    )
  }

  const submitSearch = (e) => {
    e.preventDefault();
    axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
      .then(res => {
          let resMovies = res.data.map(el=>{ return {
            id: el.id, 
            title: el.title, 
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            review: el.review,
            image_url: el.image_url
          }
        })
        let searchedMovies = resMovies.filter(
          x=> x.title.toLowerCase() === inputSearch.toLowerCase()
        ) 
        setMovies([...searchedMovies])
    })
  }

  const handleChangeSearch = (e) => {
    setInputSearch(e.target.value)
  }

  return (
    <>
    <Typography style={{textAlign: "center", fontSize: 40}}>Movies Review</Typography>
    <Paper component="form" onSubmit={submitSearch} className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search Title"
        name="title"
        inputProps={{ 'aria-label': 'description' }}
        onChange={handleChangeSearch}
        value={inputSearch}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton type="submit" className={classes.iconButton} aria-label="search">
        <SearchIcon onSubmit={submitSearch} />
      </IconButton>
    </Paper>
    <div style={{ marginTop: 20, paddingTop: 10, paddingLeft: 10 }}>
        <Grid container spacing={4} justify="center" item xs={12} sm={12}>  
            {movies !== null && movies.map((item, index)=>{
                return( 
                    <Grid item key={item.title} xs={6} sm={3}>
                        <Card className={classes.roots}>
                            <CardActionArea>     
                                <CardMedia
                                    component='img'
                                    className={classes.media}
                                    image={item.image_url}
                                    title={item.title}
                                    height='auto'
                                />
                                <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {item.title}
                                </Typography>
                                <Typography gutterBottom variant="h7" component="h3">
                                  ({item.year})
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                  {item.description.slice(0, 55)}...
                                </Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                <Action itemId={item.id} />
                            </CardActions>
                        </Card>
                    </Grid>
                )
            })}
        </Grid>
    </div>
    </>
  );
}

export default ListMovie2