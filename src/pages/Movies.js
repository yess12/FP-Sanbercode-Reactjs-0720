import React, {useState, useEffect, createContext } from "react"
import axios from "axios"
import { useHistory } from "react-router-dom"
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'no', numeric: true, disablePadding: true, label: 'No'},
  { id: 'title', numeric: false, disablePadding: true, label: 'Title' },
  { id: 'description', numeric: false, disablePadding: false, label: 'Description' },
  { id: 'year', numeric: true, disablePadding: false, label: 'Year' },
  { id: 'duration', numeric: true, disablePadding: false, label: 'Duration' },
  { id: 'genre', numeric: false, disablePadding: false, label: 'Genre' },
  { id: 'rating', numeric: true, disablePadding: false, label: 'Rating' },
  { id: 'review', numeric: false, disablePadding: true, label: 'Review' },
  { id: 'image_url', numeric: false, disablePadding: false, label: 'Image' },
  { id: 'action', numeric: false, disablePadding: true, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          Nutrition
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

const MoviesProvider = () => {
  
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [movies, setMovies] =  useState([])
  const [input, setInput]  =  useState({
    title: "",
    description: "",
    year: 2020,
    duration: 120,
    genre: "",
    rating: 0,
    review: "",
    image_url: ""
  })
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")
  const [inputFilter, setInputFilter] = useState({yearStart: 0, yearEnd: 2020, ratingStart: 0, ratingEnd: 10, durationStart: 0, durationEnd: 200})
  const [inputSearch, setInputSearch] = useState({judul: ""})

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffect( () => {
    if (movies === null){
      axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
      .then(res => {
          setMovies(res.data.map(el=>{ return {
            id: el.id, 
            title: el.title, 
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            review: el.review,
            image_url: el.image_url
          }
        }))
      })
    }
  }, [movies])

  const handleSubmit = (event) =>{
    event.preventDefault()

    let title = input.title
    console.log(input)

    if (title.replace(/\s/g,'') !== ""){      
      if (statusForm === "create"){        
        axios.post(`https://www.backendexample.sanbersy.com/api/movies`, {
          title: input.title,
          description: input.description,
          year: input.year,
          duration: input.duration,
          genre: input.genre,
          rating: parseInt(input.rating),
          review: input.review,
          image_url: input.image_url
        })
        .then(res => {
            setMovies([...movies, {id: res.data.id, ...input}])
        })
      }else if(statusForm === "edit"){
        axios.put(`https://www.backendexample.sanbersy.com/api/movies/${selectedId}`, {
          title: input.title,
          description: input.description,
          year: input.year,
          duration: input.duration,
          genre: input.genre,
          rating: parseInt(input.rating),
          review: input.review,
          image_url: input.image_url
        })
        .then(res => {
            let singleMovie = movies.find(el=> el.id === selectedId)
            singleMovie.title = input.title
            singleMovie.description = input.description
            singleMovie.year = input.year
            singleMovie.duration = input.duration
            singleMovie.genre = input.genre
            singleMovie.rating = input.rating
            singleMovie.review = input.review
            singleMovie.image_url = input.image_url
            setMovies([...movies])
        })
      }
      
      setStatusForm("create")
      setSelectedId(0)
      setInput({
        title: "",
        description: "",
        year: 2020,
        duration: 120,
        genre: "",
        rating: 0,
        review: "",
        image_url: ""
      })
    }

  }

  const Action = ({itemId}) =>{
    const handleDelete = () => {  
      let newMovies = movies.filter(el => el.id != itemId)
  
      axios.delete(`https://www.backendexample.sanbersy.com/api/movies/${itemId}`)
      .then(res => {
        console.log(res)
      })
            
      setMovies([...newMovies])
      
    }
    
    let history = useHistory();
    const handleEdit = () =>{
      let singleMovie = movies.find(x=> x.id === itemId)
      setInput({
        title: singleMovie.title,
        description: singleMovie.description,
        year: singleMovie.year,
        duration: singleMovie.duration,
        genre: singleMovie.genre,
        rating: singleMovie.rating,
        review: singleMovie.review,
        image_url: singleMovie.image_url
      })
      setSelectedId(itemId)
      setStatusForm("edit")
      history.push("/movies/edit/" + itemId)
    }

    return(
      <>
        <button type="button" onClick={handleEdit}>Edit</button>
        &nbsp;
        <button onClick={handleDelete}>Delete</button>
      </>
    )
  }

  let history = useHistory();
  const createMovie = (e) => {
    e.preventDefault();
    history.push('movies/create')

    return(
      <button onClick={createMovie}>Add Movie</button>
    )
  }

  const submitSearch = (e) => {
    e.preventDefault();
    axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
      .then(res => {
          let resMovies = res.data.map(el=>{ return {
            id: el.id, 
            title: el.title, 
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            review: el.review,
            image_url: el.image_url
          }
        })
        let searchedMovies = resMovies.filter(
          x=> x.title.toLowerCase() === inputSearch.toLowerCase()
        ) 
        setMovies([...searchedMovies])
    })
  }

  const submitFilter = (e) => {
    e.preventDefault();
    axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
      .then(res => {
          let resMovies = res.data.map(el=>{ return {
            id: el.id, 
            title: el.title, 
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            review: el.review,
            image_url: el.image_url
          }
        })
        let filteredMovies = resMovies.filter (
          x=> x.year >= inputFilter.yearStart && x.year <= inputFilter.yearEnd &&
          x.rating >= inputFilter.ratingStart && x.rating <= inputFilter.ratingEnd &&
          x.duration >= inputFilter.durationStart && x.duration <= inputFilter.durationEnd
        )
        setMovies([...filteredMovies])
    })
  }

  const handleChangeSearch = (e) => {
    setInputSearch(e.target.value)
  }

  const handleChangeFilter = (e) => {
    switch(e.target.name) {
      case "yearStart": {
        setInputFilter({...inputFilter, yearStart: e.target.value})
        break;
      }
      case "yearEnd": {
        setInputFilter({...inputFilter, yearEnd: e.target.value})
        break;
      }
      case "ratingStart": {
        setInputFilter({...inputFilter, ratingStart: e.target.value})
        break;
      }
      case "ratingEnd": {
        setInputFilter({...inputFilter, ratingEnd: e.target.value})
        break;
      }
      case "durationStart": {
        setInputFilter({...inputFilter, durationStart: e.target.value})
        break;
      }
      case "durationEnd": {
        setInputFilter({...inputFilter, durationEnd: e.target.value})
        break;
      }
      default: {
        break;
      }
    }
  }

  return(
    <>
    <div>
      <form onSubmit={submitSearch} className={classes.root} noValidate autoComplete="off">
      <label>Search Title</label>
        <input placeholder="Search Title" name="title" type="text" onChange={handleChangeSearch} value={inputSearch} inputProps={{ 'aria-label': 'description' }}/>
        <button>search</button>
      </form>
    </div>
    <br/>
    <div>
      <form onSubmit={submitFilter} className={classes.root} noValidate autoComplete="off" inputProps={{ 'aria-label': 'description' }}>
        <label>year start</label>
        <input name="yearStart" type="number" onChange={handleChangeFilter} value={inputFilter.yearStart}/>
        <label>year end</label>
        <input name="yearEnd" type="number" onChange={handleChangeFilter} value={inputFilter.yearEnd}/>
        <br/>
        <label>rating start</label>
        <input name="ratingStart" type="number" onChange={handleChangeFilter} value={inputFilter.ratingStart}/>
        <label>rating end</label>
        <input name="ratingEnd" type="number" onChange={handleChangeFilter} value={inputFilter.ratingEnd}/>
        <br/>
        <label>duration start</label>
        <input name="durationStart" type="number" onChange={handleChangeFilter} value={inputFilter.durationStart}/>
        <label>duration end</label>
        <input name="durationEnd" type="number" onChange={handleChangeFilter} value={inputFilter.durationEnd}/>
        <button>Filter</button>
        <br/> <br/>
        <button>Show Movie</button>
      </form>
    </div>
    <div>
      <form style={{display: "inline"}} onSubmit={createMovie} className={classes.root} noValidate autoComplete="off" inputProps={{ 'aria-label': 'description' }}>
        <button>Add Movie</button>
      </form>
    </div>
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={movies.length}
            />
            <TableBody>
              {movies !== null && stableSort(movies, getComparator(order, orderBy))
                .map((item, index) => {

                  return (
                    <TableRow
                      hover
                      tabIndex={-1}
                      key={index}
                    >
                      <TableCell>{index+1}</TableCell>
                      <TableCell>{item.title}</TableCell>
                      <TableCell>{item.description}</TableCell>
                      <TableCell>{item.year}</TableCell>
                      <TableCell>{item.duration}</TableCell>
                      <TableCell>{item.genre}</TableCell>
                      <TableCell>{item.rating}</TableCell>
                      <TableCell>{item.review}</TableCell>
                      <TableCell><img width="100px" src={item.image_url}/></TableCell>
                      <TableCell>
                        <Action itemId={item.id} />
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
    </>
  )

};

export default MoviesProvider

