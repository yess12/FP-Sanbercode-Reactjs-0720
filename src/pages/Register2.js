import React, { useContext, useState } from "react"
import axios from "axios"
import {UserContext} from "../context/UserContext"
import { useHistory } from "react-router-dom"

const RegisterProvider = () => {
  
  // const [, setLogin] = useContext(userContext)
  const [,setUser] = useContext(UserContext)
  const [input, setInput]  =  useState({
    username: "",
    password: "",
  })
  const [selectedId, setSelectedId]  =  useState(0)
  
  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "username":
      {
        setInput({...input, username: event.target.value});
        break
      }
      case "password":
      {
        setInput({...input, password: event.target.value});
        break
      }
    default:
      {break;}
    }
  }

  let history = useHistory();
  const handleSubmit = (event) =>{
    event.preventDefault()

    let username = input.username
    let password = input.password
    console.log(input)

    if ((username.replace(/\s/g,'') !== "") && (password.replace(/\s/g,'') !== "")) {      
      {        
        axios.post(`https://backendexample.sanbersy.com/api/users`, {
            username: input.username,
            password: input.password
        })
        .then(res => {
          setUser([{id: res.data.id, ...input}])
          localStorage.setItem("login", JSON.stringify({username: input.username, password: input.password}))
        })
      }
      
      setSelectedId(0)
      setInput({
        username: "",
        password: "",
      })
    }
    history.push("/login")
  }

  return(
    <>
      <form onSubmit={handleSubmit}>
        <label>Username: </label>
        <input type="text" name="username" onChange={handleChange} value={input.username}/>
        <br/>
        <label>Password: </label>
        <input type="password" name="password" onChange={handleChange} value={input.password}/>
        <br/>
        <button>Sign Up</button>
      </form>
    </>
  )
}

export default RegisterProvider;