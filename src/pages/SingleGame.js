import React, { useState, useEffect } from "react";
import {useParams} from "react-router-dom"
import axios from "axios"
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #6e6963 30%, #302f2d 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      maxHeight: 500,
      padding: '0 30px',
    },
    divider: {
        height: 2,
        backgroundColor: "white",
        width: 150,
    },
  });  

const SingleGame = () => {
    let { id } = useParams();
    const [games, setGames] = useState(null)
    const classes = useStyles();

    useEffect( () => {
        if (games === null){
          axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
          .then(res => {
                setGames(res.data)
          })
        }
    })

    return(
        <>
        <CssBaseline/>
        <Container maxWidth="sm">
        {games !== null &&
            <>
            <Typography className={classes.root} component="div" style={{ backgroundColor: '#0e0f05', maxHeight: '200vh', color: "white", borderRadius: 20, padding: 30, width: '100%' }}>
                <Box style={{verticalAlign: "middle"}} display="flex" p={1}>
                    <Box p={1} flexGrow={1}>
                        <Typography style={{fontFamily: "monospace"}}>
                            <h1>{games.name}</h1>
                            <Box borderColor="white" border={2} boxShadow={3}>
                                <p><img width="300px" src={games.image_url}/></p>
                            </Box>
                        </Typography>
                    </Box>
                    <Box p={1}>
                        <Typography style={{fontFamily: "monospace"}}>
                        <p><b>Genre:</b> {games.genre}</p>
                        <Divider classes={{root: classes.dividerColor}} className={classes.divider} orientation="horizontal" />
                        <p><b>Singleplayer:</b> {games.singlePlayer}</p>
                        <Divider classes={{root: classes.dividerColor}} className={classes.divider} orientation="horizontal" />
                        <p><b>Multiplayer:</b> {games.multiplayer}</p>
                        <Divider classes={{root: classes.dividerColor}} className={classes.divider} orientation="horizontal" />
                        <p><b>Platform:</b> {games.platform}</p>
                        <Divider classes={{root: classes.dividerColor}} className={classes.divider} orientation="horizontal" />
                        <p><b>Release:</b> {games.release}</p>
                        </Typography>
                    </Box>
                </Box>
            </Typography>
            </>
        }
        </Container>
        </>
    )
}

export default SingleGame