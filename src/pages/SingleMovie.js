import React, { useState, useEffect } from "react";
import {useParams} from "react-router-dom"
import axios from "axios"
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from "@material-ui/core";
import StarIcon from '@material-ui/icons/Star';

const useStyles = makeStyles((theme) => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
      marginTop: 20,
    },
    roots: {
      maxWidth: 345,
    },
      input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    divider2: {
        height: 2,
        backgroundColor: "black",
        width: 750
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

const SingleMovie = () => {
    const classes = useStyles();
    let { id } = useParams();
    const [movie, setMovie] = useState(null)

    useEffect( () => {
        if (movie === null){
          axios.get(`https://www.backendexample.sanbersy.com/api/movies/${id}`)
          .then(res => {
                setMovie(res.data)
          })
        }
    })

    return(
        <>
            {movie !== null &&
            <>
            <div style={{marginLeft: 140, marginRight: 140}}>
                <Typography variant="h5" style={{fontWeight: 700 , textAlign: "left"}}>{movie.title} ({movie.year}): Review</Typography>
                <React.Fragment>
                    <Typography variant="body1" style={{display: "inline-flex", verticalAlign: "middle", paddingRight: 10}} color="textSecondary" component="p">{movie.genre}</Typography>
                    <Divider style={{display: "inline-flex", verticalAlign: "middle"}}  className={classes.divider} orientation="vertical" />
                    <StarIcon style={{padding: 10, display: "inline-flex", verticalAlign: "middle", color: "#FFD700"}}/>
                    <Typography variant="body1" style={{display: "inline-flex", verticalAlign: "middle"}}>{movie.rating}.0</Typography>
                </React.Fragment>
                <Box display="flex" justifyContent="left">
                    <Box style={{marginTop: 25}}><img width="750px" src={movie.image_url}/></Box>
                </Box>
                <Typography variant="h6" style={{fontWeight: 700 , textAlign: "left", paddingTop: 10, paddingBottom: 10}}>Description</Typography>
                <Typography style={{width: 750, paddingBottom: 10}}> {movie.description}</Typography>
                <Divider classes={{root: classes.dividerColor}} className={classes.divider2} orientation="horizontal" />
                <Typography variant="h6" style={{fontWeight: 700 , textAlign: "left", paddingTop: 10, paddingBottom: 10}}>Review</Typography>
                <Typography style={{width: 750, paddingBottom: 10}}>{movie.review}</Typography>
                <Divider classes={{root: classes.dividerColor}} className={classes.divider2} orientation="horizontal" />
                <Typography variant="h6" style={{fontWeight: 700 , textAlign: "left", paddingTop: 10, paddingBottom: 10}}>Details</Typography>
                <Grid container>
                    <Grid item xs={3}>
                        <Typography>Release</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Typography>{movie.year}</Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography>Duration</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Typography>{movie.duration} mins</Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography>Genre</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Typography>{movie.genre}</Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography>Rating</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Typography>{movie.rating}/10</Typography>
                    </Grid>
                </Grid>
            </div>
            </>
            }
        </>
    )
}

export default SingleMovie